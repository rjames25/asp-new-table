﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="asp.netcrud.Contact" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/Assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="~/Assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
    
    
</head>
<body>

<form id="Form1" runat="server">
     <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="#">ASP_NET</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="Contact.aspx">Home <span class="sr-only">(current)</span></a>
          </li>
       </ul>
    </div>
    </nav>

      <div class="container">
    <div class="form">
    <div class="card box-shadow">
    <div class = "card-header">
        <h2>
            Save to Table
        </h2>
    </div>
    <div class="card-body">
    <asp:HiddenField ID="hfContactID" runat="server" />
        <table >
            <tr>
                <td >
                    <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
                </td>

                <td colspan="2">
                    <asp:TextBox ID="txtName" CssClass="form-control" runat="server" placeholder="e.g Renzo" ></asp:TextBox>
                </td>
            </tr>
                <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Mobile"></asp:Label>
                </td>

                <td colspan="2">
                    <asp:TextBox ID="txtMobile" CssClass="form-control"  runat="server"></asp:TextBox>
                </td>
            </tr>
                        <tr>
                <td>
                    <asp:Label ID="Label3"  runat="server" Text="Address"></asp:Label>
                </td>

                <td colspan="2">
                    <asp:TextBox ID="txtAddress" CssClass="form-control" runat="server" TextMode = "MultiLine"></asp:TextBox>
                </td>
            </tr>
                   <tr>
                <td>
                    
                </td>

                <td colspan="2">
                    <asp:Button ID="btnSave" runat="server" class="btn btn-success" Text="Save" onclick="btnSave_Click" />
                    <asp:Button ID="btnDelete" runat="server" class="btn btn-danger" Text="Delete" 
                        onclick="btnDelete_Click" />
                    <asp:Button ID="btnClear" runat="server" Text="Clear" class="btn btn-primary"
                        onclick="btnClear_Click" />
                </td>
            </tr>
              <tr>
                <td>
                    
                </td>

                <td colspan="2">
                    <asp:Label ID="lblSuccessMessage" runat="server" Text="" ForeColor="Green"></asp:Label>
                </td>
            </tr>
              <tr>
                <td>
                    
                </td>

                <td colspan="2">
                    <asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="gvContact" runat="server"  AutoGenerateColumns="false" class="table table-striped"> 
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Name" />
            <asp:BoundField DataField="Mobile" HeaderText="Mobile" />
            <asp:BoundField DataField="Address" HeaderText="Address" /> 
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkView" class="btn btn-danger" runat="server" CommandArgument= '<%# Eval("ContactID")%>' OnClick="lnk_OnClick">View</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
        </asp:GridView>
    </div>
    </form>
    </div>
    </div>
    </div>
       
    <footer class="footer">
      <div class="container">
        <span class= "text-muted"> Mendeja, Renzo Advance Programming  </span>
      </div>
   
    </footer>

    <script>        window.jQuery || document.write('<script src="Assets/js/jquery-slim.min.js"><\/script>')</script>
	<script src= "Assets/js/bootstrap.min.js" type="text/javascript"></script>

</body>
</html>
